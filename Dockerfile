FROM mmassaviol/mbb_workflows_base:latest as alltools

RUN cd /opt/biotools \
 && wget http://www.usadellab.org/cms/uploads/supplementary/Trimmomatic/Trimmomatic-0.38.zip \
 && unzip Trimmomatic-0.38.zip \
 && echo -e '#!/bin/bash java -jar /opt/biotools/Trimmomatic-0.38/trimmomatic-0.38.jar' > bin/trimmomatic \
 && chmod 777 bin/trimmomatic \
 && rm Trimmomatic-0.38.zip

RUN apt install -y fastqc=0.11.5+dfsg-6

RUN cd /opt/biotools \
 && wget https://github.com/pachterlab/kallisto/releases/download/v0.45.0/kallisto_linux-v0.45.0.tar.gz \
 && tar -zxvf kallisto_linux-v0.45.0.tar.gz \
 && mv kallisto_linux-v0.45.0/kallisto bin \
 && rm -r kallisto_linux-v0.45.0.tar.gz kallisto_linux-v0.45.0

RUN cd /opt/biotools \
 && wget https://github.com/COMBINE-lab/salmon/releases/download/v1.0.0/salmon-1.0.0_linux_x86_64.tar.gz \
 && tar -zxvf salmon-1.0.0_linux_x86_64.tar.gz \
 && cd bin && ln -s ../salmon-latest_linux_x86_64/bin/salmon salmon \
 && cd .. && rm -r salmon-1.0.0_linux_x86_64.tar.gz salmon-latest_linux_x86_64/sample_data.tgz

RUN Rscript -e 'BiocManager::install("limma", version = "3.8",Ncpus=8, clean=TRUE)'

RUN Rscript -e 'BiocManager::install("edgeR", version = "3.8",Ncpus=8, clean=TRUE)'

RUN Rscript -e 'BiocManager::install("tximport", version = "3.8",Ncpus=8, clean=TRUE)'

RUN Rscript -e 'BiocManager::install("rhdf5", version = "3.8",Ncpus=8, clean=TRUE)'

RUN Rscript -e 'BiocManager::install("GenomicFeatures", version = "3.8",Ncpus=8, clean=TRUE)'

RUN Rscript -e 'install.packages("pheatmap",Ncpus=8, clean=TRUE)'

RUN Rscript -e 'BiocManager::install("DESeq2", version = "3.8",Ncpus=8, clean=TRUE)'

RUN Rscript -e 'BiocManager::install("apeglm", version = "3.8",Ncpus=8, clean=TRUE)'

RUN Rscript -e 'BiocManager::install("BiocParallel", version = "3.8",Ncpus=8, clean=TRUE)'

ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8

#This part is necessary to run on ISEM cluster
RUN mkdir -p /share/apps/bin \
 && mkdir -p /share/apps/lib \
 && mkdir -p /share/apps/gridengine \
 && mkdir -p /share/bio \
 && mkdir -p /opt/gridengine \
 && mkdir -p /export/scrach \
 && mkdir -p /usr/lib64 \
 && ln -s /bin/bash /bin/mbb_bash \
 && ln -s /bin/bash /bin/isem_bash \
 && /usr/sbin/groupadd --system --gid 400 sge \
 && /usr/sbin/useradd --system --uid 400 --gid 400 -c GridEngine --shell /bin/true --home /opt/gridengine sge

EXPOSE 3838
CMD ["Rscript", "-e", "setwd('/sagApp/'); shiny::runApp('/sagApp/app.R',port=3838 , host='0.0.0.0')"]


FROM alltools

COPY files /workflow
COPY sagApp /sagApp

