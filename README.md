Rnaseq reads can be cleaned and/or trimmed before quantifying abundances of transcripts (with Kallisto or Salmon). Differentially expressed genes can then be checked with edgeR or deseq2. 

![alt text](./files/workflow.png "Workflow")

![alt text](./files/workflow-2.png "Workflow")